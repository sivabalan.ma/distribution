package main

import (
	_ "net/http/pprof"

	"gitlab.com/sivabalan.ma/distribution/v3/registry"
	_ "gitlab.com/sivabalan.ma/distribution/v3/registry/auth/htpasswd"
	_ "gitlab.com/sivabalan.ma/distribution/v3/registry/auth/silly"
	_ "gitlab.com/sivabalan.ma/distribution/v3/registry/auth/token"
	_ "gitlab.com/sivabalan.ma/distribution/v3/registry/proxy"
	_ "gitlab.com/sivabalan.ma/distribution/v3/registry/storage/driver/azure"
	_ "gitlab.com/sivabalan.ma/distribution/v3/registry/storage/driver/filesystem"
	_ "gitlab.com/sivabalan.ma/distribution/v3/registry/storage/driver/gcs"
	_ "gitlab.com/sivabalan.ma/distribution/v3/registry/storage/driver/inmemory"
	_ "gitlab.com/sivabalan.ma/distribution/v3/registry/storage/driver/middleware/alicdn"
	_ "gitlab.com/sivabalan.ma/distribution/v3/registry/storage/driver/middleware/cloudfront"
	_ "gitlab.com/sivabalan.ma/distribution/v3/registry/storage/driver/middleware/redirect"
	_ "gitlab.com/sivabalan.ma/distribution/v3/registry/storage/driver/oss"
	_ "gitlab.com/sivabalan.ma/distribution/v3/registry/storage/driver/s3-aws"
	_ "gitlab.com/sivabalan.ma/distribution/v3/registry/storage/driver/swift"
)

func main() {
	registry.RootCmd.Execute()
}
